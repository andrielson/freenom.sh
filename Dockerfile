FROM debian:stable-slim

ADD https://raw.githubusercontent.com/mkorthof/freenom-script/master/freenom.sh /usr/local/bin/

RUN apt-get --fix-missing --quiet --yes update && \
    apt-get --fix-missing --quiet --yes install curl locales && \
    rm --force --recursive /var/lib/apt/lists/* && \
    chmod --verbose +x /usr/local/bin/freenom.sh && \
    ln --force --symbolic --verbose /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo 'America/Sao_Paulo' | tee /etc/timezone && \
    localedef --alias-file=/usr/share/locale/locale.alias --charmap=UTF-8 --force --inputfile=pt_BR pt_BR.UTF-8
